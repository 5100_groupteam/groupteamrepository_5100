/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Order;

import java.util.ArrayList;

/**
 *
 * @author Zi Wei Fan
 */
public class OrderDirectory {
    Order order;
    ArrayList<Order> arrayListOrders;
    int sumOrderDirectoryGap;
    int sumOrderDirectoryTarget;

    public int getSumOrderDirectoryTarget(OrderDirectory od) {
        int sumOrderDirectoryTarget =0;
        for(Order o:od.getArrayListOrders()){
            sumOrderDirectoryTarget = sumOrderDirectoryTarget + o.getSumOrderTargetPrice();
        }
        return sumOrderDirectoryTarget;
    }

    public int getSumOrderDirectoryGap(OrderDirectory od) {
        int sumOdGap = 0 ;
        for(Order o:od.getArrayListOrders()){
            sumOdGap +=  o.getSumOrderGap();
        }
        return sumOdGap; 
    }

    public void setSumOrderDirectoryGap(int sumOrderDirectoryGap) {
        this.sumOrderDirectoryGap = sumOrderDirectoryGap;
    }

    public OrderDirectory() {
        this.order = order;
        arrayListOrders = new ArrayList<Order>();
        this.arrayListOrders = arrayListOrders;
    }

    public Order getOrder() {
        return order;
    }

    public ArrayList<Order> getArrayListOrders() {
        return arrayListOrders;
    }

    public void setArrayListOrders(ArrayList<Order> arrayListOrders) {
        this.arrayListOrders = arrayListOrders;
    }

    public void setOrder(Order order) {
        this.order = order;
    } 
    
    public Order CreateOrder(){
        Order order = new Order();
        return order;
    }
    
    public ArrayList<Order> addOrderToDirectory(Order order){
        arrayListOrders.add(order);
        return arrayListOrders;
    }
    
}
