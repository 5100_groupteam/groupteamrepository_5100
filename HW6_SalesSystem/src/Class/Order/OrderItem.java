/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Order;

import Class.Product.Product;

/**
 *
 * @author Zi Wei Fan
 */
public class OrderItem {
    private Product product;
    private Integer int_PaidPrice;
    private Integer int_ItemQuantity;

    public OrderItem() {
        this.product = product;
        this.int_PaidPrice = int_PaidPrice;
        this.int_ItemQuantity = int_ItemQuantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getInt_PaidPrice() {
        return int_PaidPrice;
    }

    public void setInt_PaidPrice(Integer int_PaidPrice) {
        this.int_PaidPrice = int_PaidPrice;
    }

    public Integer getInt_ItemQuantity() {
        return int_ItemQuantity;
    }

    public void setInt_ItemQuantity(Integer int_ItemQuantity) {
        this.int_ItemQuantity = int_ItemQuantity;
    }
    
    
}
