/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Order;

import Class.Person.Customer;
import Class.Person.SalesPerson;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Zi Wei Fan
 */
public class Order {
    private Boolean bool_IfOrderValid;
    private String str_OrderId;
    private ArrayList<OrderItem> ArrayListOrderItem;
    private Date date_IssueDate;
    private Date date_CompletionDate;
    private Date date_ShippingDate;
    private Customer customer;
    private SalesPerson salesPerson;

    public int getSumOrderGap() {
        int int_SumOrderPaid = this.getSumOrderPaidPrice();
        int int_SumOrderTarget = this.getSumOrderTargetPrice();
        int int_SumOrderGap =  int_SumOrderPaid-int_SumOrderTarget;
        return int_SumOrderGap;
    }

    public int getSumOrderTargetPrice() {
        int int_SumOrderTarget = 0;
        for(OrderItem oi:ArrayListOrderItem){
            int_SumOrderTarget = int_SumOrderTarget+ oi.getProduct().getInt_ProductTargetPrice()*oi.getInt_ItemQuantity();
        }
        return int_SumOrderTarget;
    }

    public void setSumOrderTargetPrice() {
        int sumTarget = 0;
        for(OrderItem oi: ArrayListOrderItem){
            sumTarget =  sumTarget + oi.getProduct().getInt_ProductTargetPrice()*oi.getInt_ItemQuantity();
        }
    }

    public int getSumOrderPaidPrice() {
        int sumOrderPaidPrice = 0;
        for(OrderItem oi: ArrayListOrderItem){
            sumOrderPaidPrice =  sumOrderPaidPrice + oi.getInt_PaidPrice()*oi.getInt_ItemQuantity();
        }
        return sumOrderPaidPrice;
    }

    public void setSumOrderPaidPrice() {
        int sum = 0;
        for(OrderItem oi: ArrayListOrderItem){
            sum =  sum + oi.getInt_PaidPrice()*oi.getInt_ItemQuantity();
        }
    }

    public Order() {
        this.bool_IfOrderValid = bool_IfOrderValid;
        this.str_OrderId = str_OrderId;
        this.ArrayListOrderItem = ArrayListOrderItem;
        ArrayListOrderItem = new ArrayList<OrderItem>();
        this.date_IssueDate = date_IssueDate;
        this.date_CompletionDate = date_CompletionDate;
        this.date_ShippingDate = date_ShippingDate;
        this.customer = customer;
        this.salesPerson = salesPerson;
    }

    public Boolean getBool_IfOrderValid() {
        return bool_IfOrderValid;
    }

    public void setBool_IfOrderValid(Boolean bool_IfOrderValid) {
        this.bool_IfOrderValid = bool_IfOrderValid;
    }

    public String getStr_OrderId() {
        return str_OrderId;
    }

    public void setStr_OrderId(String str_OrderId) {
        this.str_OrderId = str_OrderId;
    }

    public ArrayList<OrderItem> getArrayListOrderItem() {
        return ArrayListOrderItem;
    }

    public void setArrayListOrderItem(ArrayList<OrderItem> ArrayListOrderItem) {
        this.ArrayListOrderItem = ArrayListOrderItem;
    }

    public Date getDate_IssueDate() {
        return date_IssueDate;
    }

    public void setDate_IssueDate(Date date_IssueDate) {
        this.date_IssueDate = date_IssueDate;
    }

    public Date getDate_CompletionDate() {
        return date_CompletionDate;
    }

    public void setDate_CompletionDate(Date date_CompletionDate) {
        this.date_CompletionDate = date_CompletionDate;
    }

    public Date getDate_ShippingDate() {
        return date_ShippingDate;
    }

    public void setDate_ShippingDate(Date date_ShippingDate) {
        this.date_ShippingDate = date_ShippingDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public SalesPerson getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(SalesPerson salesPerson) {
        this.salesPerson = salesPerson;
    }
    
    public int getOrderItemSize(){
        return ArrayListOrderItem.size();
    }
    
    public int getOrderTotal(){
        int int_OrderTotal = 0;
        for(OrderItem orderItem : ArrayListOrderItem){
            int_OrderTotal = orderItem.getInt_PaidPrice()*orderItem.getInt_ItemQuantity();
        }
        return int_OrderTotal;
    }
    
    public OrderItem CreateOrderItem(){
        OrderItem orderItem = new OrderItem();
        return orderItem;
    }
    
    public  ArrayList<OrderItem> AddOrderItemToOrder(OrderItem orderItem){
        ArrayListOrderItem.add(orderItem);
        return ArrayListOrderItem;
    }
    
    public void RemoveOrderItemFromOrder(OrderItem orderItem){
        ArrayListOrderItem.remove(orderItem);
    }
}
