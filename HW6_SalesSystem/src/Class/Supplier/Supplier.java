/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Supplier;

import Class.Product.ProductCatalog;
import java.util.ArrayList;

/**
 *
 * @author Zi Wei Fan
 */
public class Supplier {
        private ProductCatalog productCatalog;
        private ArrayList<ProductCatalog> arrayListProductCatalog;
        private String str_SupplierName;
        private String str_Market;

    public ArrayList<ProductCatalog> getArrayListProductCatalog() {
        return arrayListProductCatalog;
    }

    public void setArrayListProductCatalog(ArrayList<ProductCatalog> arrayListProductCatalog) {
        this.arrayListProductCatalog = arrayListProductCatalog;
    }

    public String getStr_Market() {
        return str_Market;
    }

    public void setStr_Market(String str_Market) {
        this.str_Market = str_Market;
    }

    public Supplier() {
        arrayListProductCatalog = new ArrayList<ProductCatalog>();
        this.productCatalog = productCatalog;
        this.str_SupplierName = str_SupplierName;
    }

    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }

    public String getStr_SupplierName() {
        return str_SupplierName;
    }

    public void setStr_SupplierName(String str_SupplierName) {
        this.str_SupplierName = str_SupplierName;
    }
     public int getProductCatalogSize(ProductCatalog productCatalog){
         return productCatalog.getProductCatalogSize();
     }
        
     public ProductCatalog CreateProductCatalog(){
         ProductCatalog productCatalog = new ProductCatalog();
         return productCatalog;
     }
     
     public ArrayList<ProductCatalog> AddProductCatalogToSupplier(ProductCatalog productCatalog){
         arrayListProductCatalog.add(productCatalog);
         return arrayListProductCatalog;
     }
     
     public void RemoveProductCatalogFromSupplier(ProductCatalog productCatalog){
         arrayListProductCatalog.remove(productCatalog);
     }
     
     @Override
    public String toString() {
        return str_SupplierName; //To change body of generated methods, choose Tools | Templates.
    }
}
