/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Supplier;

import Class.Product.ProductCatalog;
import java.util.ArrayList;

/**
 *
 * @author Zi Wei Fan
 */
public class SupplierDirectory {
    private Supplier supplier;
    private ArrayList<Supplier> arrayListSupplier;
    private int int_SupplierDirectorySize;
    private ProductCatalog productCatalog;

    public SupplierDirectory() {
        this.supplier = supplier;
        arrayListSupplier = new ArrayList<Supplier>();
        this.int_SupplierDirectorySize = int_SupplierDirectorySize;
        this.productCatalog = productCatalog;
    }

    public ArrayList<Supplier> getArrayListSupplier() {
        return arrayListSupplier;
    }

    public void setArrayListSupplier(ArrayList<Supplier> arrayListSupplier) {
        this.arrayListSupplier = arrayListSupplier;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public int getInt_SupplierDirectorySize() {
        return arrayListSupplier.size();
    }

    public void setInt_SupplierDirectorySize(int int_SupplierDirectorySize) {
        this.int_SupplierDirectorySize = int_SupplierDirectorySize;
    }

    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }
    
    public Supplier CreateSupplier(){
        Supplier supplier = new Supplier();
        return supplier;
    }
    
    public ArrayList<Supplier> AddSupplierToDirectory(Supplier supplier){
        arrayListSupplier.add(supplier);
        return arrayListSupplier;
    }
    
    public void RemoveSupplierFromDirectory(Supplier supplier){
        arrayListSupplier.remove(supplier);
    }
    
}
