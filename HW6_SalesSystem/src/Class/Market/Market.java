/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Market;

import Class.Person.EmployeeDirectory;

/**
 *
 * @author Zi Wei Fan
 */
public class Market {
    private EmployeeDirectory customerDirectory;
    private String str_MarketTerritory;
    private int int_MonetaryValue;
    private int int_MarketBenefit;
    private String str_MarketName;
    private int int_MarketSize;
    private String str_MarketChannel;

    public Market() {
        this.customerDirectory = customerDirectory;
        this.str_MarketTerritory = str_MarketTerritory;
        this.int_MonetaryValue = int_MonetaryValue;
        this.int_MarketBenefit = int_MarketBenefit;
        this.str_MarketName = str_MarketName;
        this.int_MarketSize = int_MarketSize;
        this.str_MarketChannel = str_MarketChannel;
    }

    public EmployeeDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(EmployeeDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public String getStr_MarketTerritory() {
        return str_MarketTerritory;
    }

    public void setStr_MarketTerritory(String str_MarketTerritory) {
        this.str_MarketTerritory = str_MarketTerritory;
    }

    public int getInt_MonetaryValue() {
        return int_MonetaryValue;
    }

    public void setInt_MonetaryValue(int int_MonetaryValue) {
        this.int_MonetaryValue = int_MonetaryValue;
    }

    public int getInt_MarketBenefit() {
        return int_MarketBenefit;
    }

    public void setInt_MarketBenefit(int int_MarketBenefit) {
        this.int_MarketBenefit = int_MarketBenefit;
    }

    public String getStr_MarketName() {
        return str_MarketName;
    }

    public void setStr_MarketName(String str_MarketName) {
        this.str_MarketName = str_MarketName;
    }

    public int getInt_MarketSize() {
        return int_MarketSize;
    }

    public void setInt_MarketSize(int int_MarketSize) {
        this.int_MarketSize = int_MarketSize;
    }

    public String getStr_MarketChannel() {
        return str_MarketChannel;
    }

    public void setStr_MarketChannel(String str_MarketChannel) {
        this.str_MarketChannel = str_MarketChannel;
    }
    
    
}
