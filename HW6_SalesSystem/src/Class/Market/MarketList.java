/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Market;

import java.util.ArrayList;

/**
 *
 * @author Zi Wei Fan
 */
public class MarketList {
    private Market market;
    private ArrayList<Market> arraylistMarket;

    public MarketList() {
        this.market = market;
        arraylistMarket = new ArrayList<Market>();
        this.arraylistMarket = arraylistMarket;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public ArrayList<Market> getArraylistMarket() {
        return arraylistMarket;
    }

    public void setArraylistMarket(ArrayList<Market> arraylistMarket) {
        this.arraylistMarket = arraylistMarket;
    }
    
    public int getMarketListSize(){
        return arraylistMarket.size();
    }
    
    public Market CreateMarket(){
        Market market = new Market();
        return market;
    }
    
    public ArrayList<Market> AddMarketToList(Market market){
        arraylistMarket.add(market);
        return arraylistMarket;
    }
    
    public void RemoveMarketFromList(Market market){
        arraylistMarket.remove(market);
    }
}
