/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Market;

import java.util.ArrayList;

/**
 *
 * @author Zi Wei Fan
 */
public class MarketOfferCatalog {
    private MarketOffer marketOffer;
    private ArrayList<MarketOffer> arrayListMarketOffer;

    public MarketOfferCatalog() {
        this.marketOffer = marketOffer;
        arrayListMarketOffer = new ArrayList<MarketOffer>();
        this.arrayListMarketOffer = arrayListMarketOffer;
    }

    public MarketOffer getMarketOffer() {
        return marketOffer;
    }

    public void setMarketOffer(MarketOffer marketOffer) {
        this.marketOffer = marketOffer;
    }

    public ArrayList<MarketOffer> getArrayListMarketOffer() {
        return arrayListMarketOffer;
    }

    public void setArrayListMarketOffer(ArrayList<MarketOffer> arrayListMarketOffer) {
        this.arrayListMarketOffer = arrayListMarketOffer;
    }

    public int getMarketOfferCatalogSize(){
        return arrayListMarketOffer.size();
    }
    
    public MarketOffer CreateMarketOffer(){
        MarketOffer marketOffer = new MarketOffer();
        return marketOffer;
    }
    
    public  ArrayList<MarketOffer> AddMarketOfferToCatalog(MarketOffer marketOffer){
        arrayListMarketOffer.add(marketOffer);
        return arrayListMarketOffer;
    }
    
    public void RemoveMarketOfferFromCatalog(MarketOffer marketOffer){
        arrayListMarketOffer.remove(marketOffer);
    }
}
