/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Person;

import Class.Order.Order;
import Class.Order.OrderItem;
import java.util.ArrayList;

/**
 *
 * @author Zi Wei Fan
 */
public class Customer {
    private UserAccount userAccount;
    //private Order order;
    private ArrayList<Order> arrayListOrder;
    private String str_CustomerType;
    private Integer int_CustomerAge;
    private String str_CustomerAddress;
    private String str_CustomerHabit;
    private String str_CustomerPreferences;
    private String str_CustomerDesired;
    private int int_CustomerTotalRevenue;

    public Customer() {
        UserAccount userAccount = new UserAccount();
        this.userAccount = userAccount;
        arrayListOrder = new ArrayList<Order>();
        this.arrayListOrder = arrayListOrder;
        //this.order = order;
        this.str_CustomerType = str_CustomerType;
        this.int_CustomerAge = int_CustomerAge;
        this.str_CustomerAddress = str_CustomerAddress;
        this.str_CustomerHabit = str_CustomerHabit;
        this.str_CustomerPreferences = str_CustomerPreferences;
        this.str_CustomerDesired = str_CustomerDesired;
        this.int_CustomerTotalRevenue = int_CustomerTotalRevenue;
    }

    public ArrayList<Order> getArrayListOrder() {
        return arrayListOrder;
    }

    public void setArrayListOrder(ArrayList<Order> arrayListOrder) {
        this.arrayListOrder = arrayListOrder;
    }

    public int getInt_CustomerTotalRevenue() {
        return int_CustomerTotalRevenue;
    }

    public void setInt_CustomerTotalRevenue(int int_CustomerTotalRevenue) {
        this.int_CustomerTotalRevenue = int_CustomerTotalRevenue;
    }

    public Customer(ArrayList<Order> arrayListOrder) {
        this.arrayListOrder = arrayListOrder;
    }

    public Customer(int int_CustomerTotalRevenue) {
        this.int_CustomerTotalRevenue = int_CustomerTotalRevenue;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    /*
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
*/
    
    public String getStr_CustomerType() {
        return str_CustomerType;
    }

    public void setStr_CustomerType(String str_CustomerType) {
        this.str_CustomerType = str_CustomerType;
    }

    public Integer getInt_CustomerAge() {
        return int_CustomerAge;
    }

    public void setInt_CustomerAge(Integer int_CustomerAge) {
        this.int_CustomerAge = int_CustomerAge;
    }

    public String getStr_CustomerAddress() {
        return str_CustomerAddress;
    }

    public void setStr_CustomerAddress(String str_CustomerAddress) {
        this.str_CustomerAddress = str_CustomerAddress;
    }

    public String getStr_CustomerHabit() {
        return str_CustomerHabit;
    }

    public void setStr_CustomerHabit(String str_CustomerHabit) {
        this.str_CustomerHabit = str_CustomerHabit;
    }

    public String getStr_CustomerPreferences() {
        return str_CustomerPreferences;
    }

    public void setStr_CustomerPreferences(String str_CustomerPreferences) {
        this.str_CustomerPreferences = str_CustomerPreferences;
    }

    public String getStr_CustomerDesired() {
        return str_CustomerDesired;
    }

    public void setStr_CustomerDesired(String str_CustomerDesired) {
        this.str_CustomerDesired = str_CustomerDesired;
    }
    
    public int getCustomerOrderSize(){
        return arrayListOrder.size();
    }
    
    public Order CreateOrder(){
        Order order = new Order();
        return order;
    }
    
    public ArrayList<Order> addOrderToCustomer(Order order){
        arrayListOrder.add(order);
        return arrayListOrder;
    }
    
    public void RemoveOrderFromCustomer(Order order){
        arrayListOrder.remove(order);
    }
    
    public int getCustomerTotalRevenues(){
        int int_SumTotalRevenues = 0;
        
        for(Order order : arrayListOrder){
            int_SumTotalRevenues = int_SumTotalRevenues + order.getOrderTotal();
        }
        return int_SumTotalRevenues;
    }
}
