/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Person;

import Class.Order.Order;
import Class.Order.OrderDirectory;
import Class.Order.OrderItem;
import java.util.ArrayList;

/**
 *
 * @author Zi Wei Fan
 */
public class SalesPerson {
    //private Integer int_ProductRevenue;
    private Integer int_RevenueTotal;
    private UserAccount userAccount;
    private OrderDirectory od;
    private Customer customer;

    public SalesPerson() {
        //this.int_ProductRevenue = int_ProductRevenue;
        this.int_RevenueTotal = int_RevenueTotal;
        UserAccount userAccount = new UserAccount();
        this.userAccount = userAccount;
        this.od = od;
        this.customer = customer;
    }

     public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getInt_RevenueTotal() {
        return int_RevenueTotal;
    }

    public int setInt_RevenueTotal(Integer int_RevenueTotal, OrderDirectory od) {
        int_RevenueTotal = 0;
        for(Order o:od.getArrayListOrders()){
            int_RevenueTotal += o.getSumOrderPaidPrice();
        }
        this.int_RevenueTotal = int_RevenueTotal;
        return int_RevenueTotal;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public OrderDirectory getOrderDirectory(Customer c) {
        ArrayList<Order> arraylistOrder = c.getArrayListOrder();
        OrderDirectory orderDirectory = new OrderDirectory();
        orderDirectory.setArrayListOrders(arraylistOrder);
        return orderDirectory;
    }

    public void setOrderDirectory(OrderDirectory od) {
        this.od = od;
    }

}
