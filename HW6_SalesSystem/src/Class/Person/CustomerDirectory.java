/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Person;

import java.util.ArrayList;

/**
 *
 * @author Zi Wei Fan
 */
public class CustomerDirectory {
    private Customer customer;
    private ArrayList<Customer> arrayListCustomer;

    public CustomerDirectory() {
        this.customer = customer;
        this.arrayListCustomer = arrayListCustomer;
        arrayListCustomer = new ArrayList<Customer>();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ArrayList<Customer> getArrayListCustomer() {
        return arrayListCustomer;
    }

    public void setArrayListCustomer(ArrayList<Customer> arrayListCustomer) {
        this.arrayListCustomer = arrayListCustomer;
    }

    public Customer CreateCustomer(){
        Customer customer = new Customer();
        return customer;
    }
    
    public ArrayList<Customer> addCustomerToArray(Customer customer){
        arrayListCustomer.add(customer);
        return arrayListCustomer;
    }
    
    public void removeCustomerFromArray(Customer customer){
        arrayListCustomer.remove(customer);
    }
    
    public int getCustomerDirectorySize(ArrayList<Customer> arrayListCustomer){
        return arrayListCustomer.size();
    }   
}
