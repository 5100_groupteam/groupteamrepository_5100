/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Person;

/**
 *
 * @author Zi Wei Fan
 */
public class Person {
    private String str_PersonFirstName;
    private String str_PersonLastName;
    private String str_PersonContact;
    private String str_PersonUsername;

    public Person() {
        this.str_PersonFirstName = str_PersonFirstName;
        this.str_PersonLastName = str_PersonLastName;
        this.str_PersonContact = str_PersonContact;
        this.str_PersonUsername = str_PersonUsername;
    }

    public String getStr_PersonUsername() {
        return str_PersonUsername;
    }

    public void setStr_PersonUsername(String str_PersonUsername) {
        this.str_PersonUsername = str_PersonUsername;
    }

    public String getStr_PersonFirstName() {
        return str_PersonFirstName;
    }

    public void setStr_PersonFirstName(String str_PersonFirstName) {
        this.str_PersonFirstName = str_PersonFirstName;
    }

    public String getStr_PersonLastName() {
        return str_PersonLastName;
    }

    public void setStr_PersonLastName(String str_PersonLastName) {
        this.str_PersonLastName = str_PersonLastName;
    }

    public String getStr_PersonContact() {
        return str_PersonContact;
    }

    public void setStr_PersonContact(String str_PersonContact) {
        this.str_PersonContact = str_PersonContact;
    }
    
    @Override
    public String toString(){
        return this.str_PersonUsername;
    }
}
