/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Person;

/**
 *
 * @author Zi Wei Fan
 */
public class UserAccount {
    private Person person;
    private String str_AccountRole;
    private String str_RoleStatus;
    private String str_Password;

    public UserAccount() {
        Person person = new Person();
        this.person = person;
        this.str_AccountRole = str_AccountRole;
        this.str_RoleStatus = str_RoleStatus;
        this.str_Password = str_Password;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getStr_AccountRole() {
        return str_AccountRole;
    }

    public void setStr_AccountRole(String str_AccountRole) {
        this.str_AccountRole = str_AccountRole;
    }

    public String getStr_RoleStatus() {
        return str_RoleStatus;
    }

    public void setStr_RoleStatus(String str_RoleStatus) {
        this.str_RoleStatus = str_RoleStatus;
    }

    public String getStr_Password() {
        return str_Password;
    }

    public void setStr_Password(String str_Password) {
        this.str_Password = str_Password;
    }
    
    
}
