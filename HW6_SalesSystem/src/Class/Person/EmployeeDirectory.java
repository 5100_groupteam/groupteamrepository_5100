/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Person;

import java.util.ArrayList;

/**
 *
 * @author Zi Wei Fan
 */
public class EmployeeDirectory {
    //private SalesPerson salesPerson;
    private Integer int_SalesPersonCatalogSize;
    private ArrayList<SalesPerson> arrayListSalesPerson;

    public EmployeeDirectory() {
        arrayListSalesPerson = new ArrayList<SalesPerson>();
        //this.salesPerson = salesPerson;
        this.int_SalesPersonCatalogSize = int_SalesPersonCatalogSize;
        this.arrayListSalesPerson = arrayListSalesPerson;
    }

    /*
    public SalesPerson getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(SalesPerson salesPerson) {
        this.salesPerson = salesPerson;
    }
*/
    public Integer getInt_SalesPersonCatalogSize() {
        return arrayListSalesPerson.size();
    }

    public void setInt_SalesPersonCatalogSize(Integer int_SalesPersonCatalogSize) {
        this.int_SalesPersonCatalogSize = int_SalesPersonCatalogSize;
    }

    public ArrayList<SalesPerson> getArrayListSalesPerson() {
        return arrayListSalesPerson;
    }

    public void setArrayListSalesPerson(ArrayList<SalesPerson> arrayListSalesPerson) {
        this.arrayListSalesPerson = arrayListSalesPerson;
    }
    
    public SalesPerson CreateSalesPerson(){
        SalesPerson salesPerson = new SalesPerson();
        return salesPerson;
    }
    
    public ArrayList<SalesPerson> AddSalesPersonToDirectory(SalesPerson salesPerson){
        arrayListSalesPerson.add(salesPerson);
        return arrayListSalesPerson;
    }
    
    public void RemoveSalesPersonFromDirectory(SalesPerson salesPerson){
        arrayListSalesPerson.remove(salesPerson);
    }
}
