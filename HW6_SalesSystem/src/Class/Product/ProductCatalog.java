/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Product;

import java.util.ArrayList;

/**
 *
 * @author Zi Wei Fan
 */
public class ProductCatalog {
    private Product product;
    private ArrayList<Product> arrayListProduct;
    private Integer int_ProductCatalog;
    private Integer int_ProductCatalogTargetSum;
    private Integer int_ProductCatalogActualSum;

    public ProductCatalog() {
        arrayListProduct = new ArrayList<Product>(); 
        this.int_ProductCatalog = int_ProductCatalog;
        this.int_ProductCatalogTargetSum = int_ProductCatalogTargetSum;
        this.int_ProductCatalogActualSum = int_ProductCatalogActualSum;
    }

    public Integer getInt_ProductCatalog() {
        return int_ProductCatalog;
    }

    public void setInt_ProductCatalog(Integer int_ProductCatalog) {
        this.int_ProductCatalog = int_ProductCatalog;
    }

    public Integer getInt_ProductCatalogTargetSum() {
        return int_ProductCatalogTargetSum;
    }

    public void setInt_ProductCatalogTargetSum(Integer int_ProductCatalogTargetSum) {
        this.int_ProductCatalogTargetSum = int_ProductCatalogTargetSum;
    }

    public Integer getInt_ProductCatalogActualSum() {
        return int_ProductCatalogActualSum;
    }

    public void setInt_ProductCatalogActualSum(Integer int_ProductCatalogActualSum) {
        this.int_ProductCatalogActualSum = int_ProductCatalogActualSum;
    }
    
     public Product CreateProduct(){
         Product product = new Product();
         return product;
   }
      public ArrayList<Product> addProductToCatalog(Product product){
          arrayListProduct.add(product);
          return arrayListProduct;
      }
      
      public void removeProductToCatalog(Product product){
          arrayListProduct.remove(product);
      }
      
    public int getProductCatalogSize(){
        return arrayListProduct.size();
    }
    
    public ArrayList<Product> getArrayListProduct(){
        return arrayListProduct;
    }
}
