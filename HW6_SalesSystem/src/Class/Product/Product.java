/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class.Product;

/**
 *
 * @author Zi Wei Fan
 */
public class Product {
    private String str_ProductName;
    private String str_ProductId;
    private Boolean bool_ProductAvailable;
    private Integer int_ProductTargetPrice;
    private Integer int_ProductFloorPrice;
    private Integer int_ProductCeilingPrice;
    private String str_ProductDescription;
    private String str_Supplier;
 
    public Product() {
        this.str_ProductName = str_ProductName;
        this.str_ProductId = str_ProductId;
        this.bool_ProductAvailable = bool_ProductAvailable;
        this.int_ProductTargetPrice = int_ProductTargetPrice;
        this.int_ProductFloorPrice = int_ProductFloorPrice;
        this.int_ProductCeilingPrice = int_ProductCeilingPrice;
        this.str_ProductDescription = str_ProductDescription;
        this.str_Supplier = str_Supplier;
    }

        public String getStr_Supplier() {
        return str_Supplier;
    }

    public void setStr_Supplier(String str_Supplier) {
        this.str_Supplier = str_Supplier;
    }
    
    public String getStr_ProductName() {
        return str_ProductName;
    }

    public void setStr_ProductName(String str_ProductName) {
        this.str_ProductName = str_ProductName;
    }

    public String getStr_ProductId() {
        return str_ProductId;
    }

    public void setStr_ProductId(String str_ProductId) {
        this.str_ProductId = str_ProductId;
    }

    public Boolean getBool_ProductAvailable() {
        return bool_ProductAvailable;
    }

    public void setBool_ProductAvailable(Boolean bool_ProductAvailable) {
        this.bool_ProductAvailable = bool_ProductAvailable;
    }

    public Integer getInt_ProductTargetPrice() {
        return int_ProductTargetPrice;
    }

    public void setInt_ProductTargetPrice(Integer int_ProductTargetPrice) {
        this.int_ProductTargetPrice = int_ProductTargetPrice;
    }

    public Integer getInt_ProductFloorPrice() {
        return int_ProductFloorPrice;
    }

    public void setInt_ProductFloorPrice(Integer int_ProductFloorPrice) {
        this.int_ProductFloorPrice = int_ProductFloorPrice;
    }

    public Integer getInt_ProductCeilingPrice() {
        return int_ProductCeilingPrice;
    }

    public void setInt_ProductCeilingPrice(Integer int_ProductCeilingPrice) {
        this.int_ProductCeilingPrice = int_ProductCeilingPrice;
    }

    public String getStr_ProductDescription() {
        return str_ProductDescription;
    }

    public void setStr_ProductDescription(String str_ProductDescription) {
        this.str_ProductDescription = str_ProductDescription;
    }

}
