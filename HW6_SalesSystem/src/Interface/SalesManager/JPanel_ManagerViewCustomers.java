/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.SalesManager;

import Class.Person.Customer;
import Class.Person.CustomerDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Zi Wei Fan
 */
public class JPanel_ManagerViewCustomers extends javax.swing.JPanel {

    CustomerDirectory customerDirectory;
    Customer  customer;
    JPanel userProcessControl;
    
    public JPanel_ManagerViewCustomers(JPanel userProcessControl, CustomerDirectory customerDirectory) {
        this.userProcessControl = userProcessControl;
        this.customerDirectory = customerDirectory;
        initComponents();
        populate(customerDirectory);
    }

    public void populate(CustomerDirectory customerDirectory){
        int rowCount = tbl_ManagerViewCustomer.getRowCount();
        DefaultTableModel model = (DefaultTableModel)tbl_ManagerViewCustomer.getModel();
        
        for(int i=rowCount-1; i>=0; i--){
            model.removeRow(i);
        }
        for(Customer c:customerDirectory.getArrayListCustomer()){
            Object row[] = new Object[9];
            row[0] = c.getUserAccount().getPerson().getStr_PersonUsername();
            row[1] = c.getUserAccount().getPerson().getStr_PersonFirstName();
            row[2] = c.getUserAccount().getPerson().getStr_PersonLastName();
            row[3] = c.getInt_CustomerAge();
            row[4] = c.getStr_CustomerPreferences().toString();
            row[5] = c.getStr_CustomerAddress().toString();
            row[6] = c.getStr_CustomerDesired().toString();
            row[7] = c.getStr_CustomerHabit().toString();
            row[8] = c.getStr_CustomerType().toString();
            model.addRow(row);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label1 = new java.awt.Label();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_ManagerViewCustomer = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        btn_Back = new javax.swing.JButton();
        btn_ManagerViewRevenue = new javax.swing.JButton();
        btn_Create = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 204, 204));

        label1.setFont(new java.awt.Font("Dialog", 0, 48)); // NOI18N
        label1.setText("Sales Manager View Customers");

        tbl_ManagerViewCustomer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Username", "Firstname", "Lastname", "Age", "Preferences", "Address", "Desired", "Habit", "Type"
            }
        ));
        jScrollPane1.setViewportView(tbl_ManagerViewCustomer);

        jButton1.setText("Delete");

        jButton2.setText("Update");

        btn_Back.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btn_Back.setText("<< Back");
        btn_Back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_BackActionPerformed(evt);
            }
        });

        btn_ManagerViewRevenue.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btn_ManagerViewRevenue.setText("View Revenue >>");
        btn_ManagerViewRevenue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ManagerViewRevenueActionPerformed(evt);
            }
        });

        btn_Create.setText("Create");
        btn_Create.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CreateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_ManagerViewRevenue, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btn_Back, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 978, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2)
                    .addComponent(jButton1)
                    .addComponent(btn_Create))
                .addGap(51, 51, 51))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 872, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(202, 202, 202))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(btn_Create)
                        .addGap(49, 49, 49)
                        .addComponent(jButton2)
                        .addGap(50, 50, 50)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane1)
                    .addComponent(btn_Back, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(58, 58, 58)
                .addComponent(btn_ManagerViewRevenue, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btn_BackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_BackActionPerformed
        userProcessControl.remove(this);
        CardLayout layout = (CardLayout)userProcessControl.getLayout();
        layout.previous(userProcessControl);
    }//GEN-LAST:event_btn_BackActionPerformed

    private void btn_ManagerViewRevenueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ManagerViewRevenueActionPerformed
         
    }//GEN-LAST:event_btn_ManagerViewRevenueActionPerformed

    private void btn_CreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CreateActionPerformed
        JPanel_ManagerCustomer awajp = new JPanel_ManagerCustomer(userProcessControl, customerDirectory, customer);
        userProcessControl.add("JPanel_ManagerCustomer", awajp);
        CardLayout layout = (CardLayout)userProcessControl.getLayout();
        layout.next(userProcessControl);
    }//GEN-LAST:event_btn_CreateActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Back;
    private javax.swing.JButton btn_Create;
    private javax.swing.JButton btn_ManagerViewRevenue;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.Label label1;
    private javax.swing.JTable tbl_ManagerViewCustomer;
    // End of variables declaration//GEN-END:variables
}
