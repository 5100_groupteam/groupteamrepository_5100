/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.SalesManager;

import Business.SalesPersonComparator;
import Class.Market.MarketOfferCatalog;
import Class.Order.OrderDirectory;
import Class.Person.CustomerDirectory;
import Class.Person.EmployeeDirectory;
import Class.Person.SalesPerson;
import Class.Product.ProductCatalog;
import java.awt.CardLayout;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Zi Wei Fan
 */
public class JPanel_ManagerViewPerformance extends javax.swing.JPanel {

    private CustomerDirectory customerDirectory;
    private EmployeeDirectory employeeDirectory;
    private OrderDirectory orderDirectory;
    private MarketOfferCatalog marketOfferCatalog;
    private ProductCatalog productCatalog;
    JPanel userProcessControl;
    private ArrayList<String> arrayListGap;
    
    public JPanel_ManagerViewPerformance(JPanel userProcessControl, CustomerDirectory customerDirectory, EmployeeDirectory employeeDirectory, ProductCatalog productCatalog, OrderDirectory orderDirectory) {
        initComponents();
        this.userProcessControl = userProcessControl;
        this.customerDirectory = customerDirectory;
        this.employeeDirectory = employeeDirectory;
        this.productCatalog = productCatalog;
        this.orderDirectory = orderDirectory;
       
        populate(employeeDirectory);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label1 = new java.awt.Label();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lbl_TotalRevenue = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_ViewSalesPersonRevenue = new javax.swing.JTable();
        btn_Back = new javax.swing.JButton();
        Combox_Rank = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        btn_Rank = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 204, 204));

        label1.setFont(new java.awt.Font("Dialog", 0, 48)); // NOI18N
        label1.setText("Sales Manager View Performance");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Total Revenue: ");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 0, 102));
        jLabel2.setText("$");

        lbl_TotalRevenue.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lbl_TotalRevenue.setForeground(new java.awt.Color(255, 51, 102));
        lbl_TotalRevenue.setText("$");

        tbl_ViewSalesPersonRevenue.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Sales Username", "Sales Firstname", "Sales Lastname", "Total Revenue", "Target", "Gap"
            }
        ));
        jScrollPane1.setViewportView(tbl_ViewSalesPersonRevenue);

        btn_Back.setText("<< Back");
        btn_Back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_BackActionPerformed(evt);
            }
        });

        Combox_Rank.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Combox_Rank.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Top10 Total Revenue", ">Target", "<Target" }));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Select Rank:");

        btn_Rank.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btn_Rank.setText("Rank");
        btn_Rank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_RankActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(152, 152, 152)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_TotalRevenue, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(94, 94, 94)
                        .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 872, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(221, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_Back, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 936, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn_Rank)
                            .addComponent(Combox_Rank, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(lbl_TotalRevenue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btn_Back, javax.swing.GroupLayout.DEFAULT_SIZE, 442, Short.MAX_VALUE)
                        .addGap(114, 114, 114))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Combox_Rank, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btn_Rank)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents
    public void populate(EmployeeDirectory employeeDirectory){
        int rowCount = tbl_ViewSalesPersonRevenue.getRowCount();
        DefaultTableModel model = (DefaultTableModel)tbl_ViewSalesPersonRevenue.getModel();
        
        for(int i=rowCount-1; i>=0; i--){
            model.removeRow(i);
        }
        
        int int_TotalRevenue = 0;
        //arrayListGap = new ArrayList<String>();
        for(SalesPerson sp: employeeDirectory.getArrayListSalesPerson()){
            Object row[] = new Object[6];
            OrderDirectory od = sp.getOrderDirectory(sp.getCustomer());
            int sumOrderDirectory = sp.getOrderDirectory(sp.getCustomer()).getSumOrderDirectoryGap(od);
            int int_RevenueTotal = sp.setInt_RevenueTotal(sumOrderDirectory,od);
            
            row[0] = sp.getUserAccount().getPerson().getStr_PersonUsername();
            row[1] = sp.getUserAccount().getPerson().getStr_PersonFirstName().toString();
            row[2] = sp.getUserAccount().getPerson().getStr_PersonLastName().toString();
            row[3] = Integer.toString(sp.getInt_RevenueTotal()); 
            row[4] = sp.getOrderDirectory(sp.getCustomer()).getSumOrderDirectoryTarget(od);
            row[5] = sp.getOrderDirectory(sp.getCustomer()).getSumOrderDirectoryGap(od);
            
            model.addRow(row);
            //arrayListGap.add(row[5].toString());
            int_TotalRevenue+= sp.getInt_RevenueTotal();
        }
        lbl_TotalRevenue.setText(Integer.toString(int_TotalRevenue));
    }
    
    private void btn_BackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_BackActionPerformed
        userProcessControl.remove(this);
        CardLayout layout = (CardLayout)userProcessControl.getLayout();
        layout.previous(userProcessControl);
    }//GEN-LAST:event_btn_BackActionPerformed

    private void btn_RankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_RankActionPerformed
        if(Combox_Rank.getSelectedItem().toString().equals("Top10 Total Revenue") ){
            //Choose TOP10 from GAP.
            Collections.sort(employeeDirectory.getArrayListSalesPerson(), new SalesPersonComparator()); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            populate(employeeDirectory);
        }
       
        //Get > Gap.
        if(Combox_Rank.getSelectedItem().toString().equals(">Target")){
            EmployeeDirectory edMorethanTarget = new EmployeeDirectory();
             int int_spRevenueTotal = 0;
             int int_spTarget = 0;
             
            for(SalesPerson sp: employeeDirectory.getArrayListSalesPerson()){
                OrderDirectory od = sp.getOrderDirectory(sp.getCustomer());
                int_spRevenueTotal = sp.getInt_RevenueTotal();
                int_spTarget = sp.getOrderDirectory(sp.getCustomer()).getSumOrderDirectoryTarget(od);
                if(int_spRevenueTotal>int_spTarget){
                    edMorethanTarget.AddSalesPersonToDirectory(sp);
                }
            }
            populate(edMorethanTarget);
        }
        //Get < Gap.
         if(Combox_Rank.getSelectedItem().toString().equals("<Target")){
            EmployeeDirectory edMorethanTarget = new EmployeeDirectory();
             int int_spRevenueTotal = 0;
             int int_spTarget = 0;
             
            for(SalesPerson sp: employeeDirectory.getArrayListSalesPerson()){
                OrderDirectory od = sp.getOrderDirectory(sp.getCustomer());
                int_spRevenueTotal = sp.getInt_RevenueTotal();
                int_spTarget = sp.getOrderDirectory(sp.getCustomer()).getSumOrderDirectoryTarget(od);
                if(int_spRevenueTotal<int_spTarget){
                    edMorethanTarget.AddSalesPersonToDirectory(sp);
                }
            }
            populate(edMorethanTarget);
        }
    }//GEN-LAST:event_btn_RankActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> Combox_Rank;
    private javax.swing.JButton btn_Back;
    private javax.swing.JButton btn_Rank;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.Label label1;
    private javax.swing.JLabel lbl_TotalRevenue;
    private javax.swing.JTable tbl_ViewSalesPersonRevenue;
    // End of variables declaration//GEN-END:variables
}
