/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hw6_salessystem;

import Class.Market.Market;
import Class.Market.MarketList;
import Class.Market.MarketOffer;
import Class.Person.Customer;
import Class.Person.CustomerDirectory;
import Class.Person.EmployeeDirectory;
import Class.Product.Product;
import Class.Product.ProductCatalog;
import Class.Market.MarketOfferCatalog;
import Class.Order.Order;
import Class.Order.OrderDirectory;
import Class.Order.OrderItem;
import Class.Person.SalesPerson;
import Class.Person.UserAccount;
import Class.Supplier.Supplier;
import Class.Supplier.SupplierDirectory;
import Interface.SalesManager.JPanel_ManagerMenu;
import Interface.SalesManager.JPanel_ManagerViewCustomers;
import Interface.SalesManager.JPanel_ManagerViewPerformance;
import java.awt.CardLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class JFrame extends javax.swing.JFrame {

    private Customer  customer;
    private Product product;
    private Market market;
    private MarketOffer marketOffer;
    private SalesPerson salesPerson;
    
    private OrderDirectory orderDirectory;
    private MarketOfferCatalog marketOfferCatalog;
    private CustomerDirectory customerDirectory;
    private EmployeeDirectory employeeDirectory;
    private SupplierDirectory supplierDirectory;
    private ProductCatalog productCatalog;
    private MarketList marketList;
    ProductCatalog productCatalog_HP;
    ProductCatalog productCatalog_Apple;
    ProductCatalog productCatalog_Dell;
    
    //JPanel userProcessControl1;
 
    public JFrame() {
        initComponents();
        
        userProcessControl.setVisible(false);
        Random rand = new Random();
                
        customerDirectory = new CustomerDirectory();
        productCatalog = new ProductCatalog();
        marketList = new MarketList();
        marketOfferCatalog = new MarketOfferCatalog();
        orderDirectory = new OrderDirectory();   
        supplierDirectory = new SupplierDirectory();
        //ArrayList<SalesPerson> arrayListEmployeeDirectory = new ArrayList<SalesPerson>();
        
         //Initiate 13 Employees--Sales Person.
        employeeDirectory = new EmployeeDirectory();
         
         for(int count = 0; count<13;count++){
            SalesPerson  salesPerson= employeeDirectory.CreateSalesPerson();
            salesPerson.getUserAccount().getPerson().setStr_PersonUsername("employee"+count);
            salesPerson.getUserAccount().getPerson().setStr_PersonFirstName("employee"+count);
            salesPerson.getUserAccount().getPerson().setStr_PersonLastName("employee"+count);
            salesPerson.getUserAccount().getPerson().setStr_PersonContact(Integer.toString((int) (Math.random() * 100))); //TODO: 6 digits.
            salesPerson.getUserAccount().setStr_AccountRole("SalesPerson");
            employeeDirectory.AddSalesPersonToDirectory(salesPerson);
            //employeeDirectory.setArrayListSalesPerson(employeeDirectory.AddSalesPersonToDirectory(salesPerson));
            
         }
          //Initiate Supplier:
        Supplier supplier_HP = new Supplier();
        Supplier supplier_Apple = new Supplier();
        //Supplier supplier_Lenovo = new Supplier();
        Supplier supplier_Dell = new Supplier();
        
        supplier_HP.setStr_SupplierName("HP");
        supplier_Apple.setStr_SupplierName("Apple");
        //supplier_Lenovo.setStr_SupplierName("Lenovo");
        supplier_Dell.setStr_SupplierName("Dell");
        
        supplierDirectory.AddSupplierToDirectory(supplier_HP);
        supplierDirectory.AddSupplierToDirectory(supplier_Apple);
        //supplierDirectory.AddSupplierToDirectory(supplier_Lenovo);
        supplierDirectory.AddSupplierToDirectory(supplier_Dell);
        
        productCatalog_HP = supplier_HP.CreateProductCatalog();
        productCatalog_Apple = supplier_Apple.CreateProductCatalog();
        productCatalog_Dell = supplier_Dell.CreateProductCatalog();
        
        //Initiate Product:
         for(int count = 0; count < 26; count++){
          Product product = productCatalog_HP.CreateProduct(); //Initiate HP.
          product.setStr_ProductName("Laptop110"+count);
          product.setStr_ProductId("hplaptop110"+count);
          product.setBool_ProductAvailable(true);
          product.setInt_ProductFloorPrice(1000);
          product.setInt_ProductCeilingPrice(1500);
          product.setInt_ProductTargetPrice(1400);
          product.setStr_Supplier("HP");
          product.setStr_ProductDescription("This is for HP Laptop. "+ product.getStr_ProductId());
          productCatalog_HP.addProductToCatalog(product);
          productCatalog.addProductToCatalog(product);
        }
         
         for(int count = 0; count < 26; count++){
          Product product = productCatalog_Apple.CreateProduct(); //Initiate Apple.
          product.setStr_ProductName("Mac_"+count);
          product.setStr_ProductId("applemac"+count);
          product.setBool_ProductAvailable(true);
          product.setInt_ProductFloorPrice(1300);
          product.setInt_ProductCeilingPrice(1800);
          product.setInt_ProductTargetPrice(1600);
          product.setStr_Supplier("Apple");
          product.setStr_ProductDescription("This is for AppleMac." + product.getStr_ProductId());
          productCatalog_Apple.addProductToCatalog(product);
          productCatalog.addProductToCatalog(product);
        }
        
         for(int count = 0; count < 26; count++){
          Product product = productCatalog_Dell.CreateProduct();
          product.setStr_ProductName("Dell_"+count);
          product.setStr_ProductId("delllaptop"+count);
          product.setBool_ProductAvailable(true);
          product.setInt_ProductFloorPrice(1100);
          product.setInt_ProductCeilingPrice(1700);
          product.setInt_ProductTargetPrice(1500);
          product.setStr_Supplier("Dell");
          product.setStr_ProductDescription("This is for Dell." + product.getStr_ProductId());
          productCatalog_Dell.addProductToCatalog(product);
          productCatalog.addProductToCatalog(product);
        }
          //Initiate Customer;
        String[] strArray_CustomerType = {"Education","Finance"};
        String[] strArray_CustomerPreference = {"Color","Price","Supplier"};
        String[] strArray_CustomerDesired = {"Low Price", "Good Feedback", "Quantity", "Quality"};
        String[] strArray_CustomerHabit = {"PriceFirst", "SupplierFirst", "BrandFirst"};
        String[] strArray_CustomerAddress = {"No.150,Exchange Street,Malden", "No.110,Exchange Street,Malden", "No.170,Exchange Street,Malden", "No.130,Exchange Street,Revere"};
        
        //loop:
        for(int count=0; count<13; count++){
            Customer customer = customerDirectory.CreateCustomer();
            OrderDirectory  odCustomer = new OrderDirectory();
            customer.getUserAccount().getPerson().setStr_PersonUsername("customer"+count);
            customer.getUserAccount().getPerson().setStr_PersonFirstName("VIPcustomer"+count);
            customer.getUserAccount().getPerson().setStr_PersonLastName("VIPcustomer"+count);
            customer.getUserAccount().getPerson().setStr_PersonContact(Integer.toString((int) (Math.random() * 10))); //Random Contact;
            customer.setInt_CustomerAge(rand.nextInt(50)); //Random age.
            
            String str_RandomType = strArray_CustomerType[(int)(Math.random()*strArray_CustomerType.length)];
            String str_RandomPreferences = strArray_CustomerPreference[(int)(Math.random()*strArray_CustomerPreference.length)];
            String str_RandomDesired = strArray_CustomerDesired[(int)(Math.random()*strArray_CustomerDesired.length)];
            String str_RandomHabit = strArray_CustomerHabit[(int)(Math.random()*strArray_CustomerHabit.length)];
            String str_RandomAddress = strArray_CustomerAddress[(int)(Math.random()*strArray_CustomerAddress.length)];
            
            customer.setStr_CustomerType(str_RandomType);//Random from 'Education', 'Banking.'
            customer.setStr_CustomerPreferences(str_RandomPreferences);//Random from Preferences.
            customer.setStr_CustomerDesired(str_RandomDesired);
            customer.setStr_CustomerHabit(str_RandomHabit); //Random Habit.
            customer.setStr_CustomerAddress(str_RandomAddress);//Random Address.
            customerDirectory.setArrayListCustomer(customerDirectory.addCustomerToArray(customer));        
     
         for(int countOrder=0; countOrder<customerDirectory.getArrayListCustomer().size(); countOrder++){
                Order order = customer.CreateOrder();
                order.setStr_OrderId("order"+"_"+customer.getUserAccount().getPerson().getStr_PersonUsername()+"_"+countOrder);
                order.setDate_IssueDate(getRondomDate("2017-07-02", "2017-07-30"));
                order.setDate_CompletionDate(getRondomDate("2017-07-30", "2017-08-30"));
                order.setDate_ShippingDate(getRondomDate("2017-08-30", "2017-09-30"));
                order.setBool_IfOrderValid(true);
                order.setCustomer(customer);
                odCustomer.addOrderToDirectory(order);
                customer.setArrayListOrder(odCustomer.getArrayListOrders());  
                
                //Initiate 3 OrderItems and add to Order.
              for(Order orderCustomer: odCustomer.getArrayListOrders()){
                    OrderItem orderItem1 = orderCustomer.CreateOrderItem();
                    OrderItem orderItem2 = orderCustomer.CreateOrderItem();
                    OrderItem orderItem3 = orderCustomer.CreateOrderItem();
                    
                    orderItem1.setProduct(productCatalog_Dell.getArrayListProduct().get(countOrder));
                    orderItem2.setProduct(productCatalog_HP.getArrayListProduct().get(countOrder));
                    orderItem3.setProduct(productCatalog_Apple.getArrayListProduct().get(countOrder));
                    orderCustomer.AddOrderItemToOrder(orderItem1);
                    orderCustomer.AddOrderItemToOrder(orderItem2);
                    orderCustomer.AddOrderItemToOrder(orderItem3);
                        
                    orderItem1.setInt_ItemQuantity(2);
                    orderItem2.setInt_ItemQuantity(1);
                    orderItem3.setInt_ItemQuantity(1);
                    
                    orderItem1.setInt_PaidPrice(getRandom(500, 3000));
                    orderItem2.setInt_PaidPrice(getRandom(500, 3000));
                    orderItem3.setInt_PaidPrice(getRandom(400, 3000));
                    //employeeDirectory.getArrayListSalesPerson().get(count).setOrderDirectory(orderDirectory);
              }           
          } 
   //TODO: Add customer to Salesperson.
                /*Assign orderDirectory to salesPerson.
                for(int countCustomer= 0; countCustomer<customerDirectory.getArrayListCustomer().size(); countCustomer++){
                    employeeDirectory.getArrayListSalesPerson().get(countCustomer).setOrderDirectory(orderDirectory);
                }*/
                //employeeDirectory.getArrayListSalesPerson().get(count).setOrderDirectory(orderDirectory);      
        }
        
        for(int count = 0; count<13;count++){
            salesPerson = employeeDirectory.getArrayListSalesPerson().get(count);
            salesPerson.setCustomer(customerDirectory.getArrayListCustomer().get(count));
         }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel_Left = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txt_managerUsername = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txt_managerPassword = new javax.swing.JTextField();
        btn_Login = new javax.swing.JButton();
        userProcessControl = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel_Left.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Sales Manager Please Login:");
        jPanel_Left.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 192, 375, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Username:");
        jPanel_Left.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(54, 315, -1, -1));

        txt_managerUsername.setText("salesmanager");
        jPanel_Left.add(txt_managerUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(166, 312, 163, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Password:");
        jPanel_Left.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(58, 421, -1, -1));

        txt_managerPassword.setText("123");
        jPanel_Left.add(txt_managerPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(171, 420, 155, -1));

        btn_Login.setText("Login");
        btn_Login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_LoginActionPerformed(evt);
            }
        });
        jPanel_Left.add(btn_Login, new org.netbeans.lib.awtextra.AbsoluteConstraints(136, 520, -1, -1));

        jSplitPane1.setLeftComponent(jPanel_Left);

        userProcessControl.setLayout(new java.awt.CardLayout());
        jSplitPane1.setRightComponent(userProcessControl);

        getContentPane().add(jSplitPane1, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_LoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_LoginActionPerformed
        UserAccount manager = new UserAccount();
        manager.getPerson().setStr_PersonUsername("salesmanager");
        manager.getPerson().setStr_PersonFirstName("Sales");
        manager.getPerson().setStr_PersonLastName("Manager");
        manager.setStr_Password("123");

        String str_inputUsername = txt_managerUsername.getText().toString().trim();
        String str_inputPassword = txt_managerPassword.getText().toString().trim();

        if((str_inputUsername.equals(manager.getPerson().getStr_PersonUsername()))&&(str_inputPassword.equals(manager.getStr_Password()))) {
            userProcessControl.setVisible(true);
            txt_managerUsername.setEnabled(false);
            txt_managerPassword.setEnabled(false);
            btn_Login.setEnabled(false);
            
                JPanel_ManagerMenu awajp = new JPanel_ManagerMenu(userProcessControl, customerDirectory, 
                                                                                                            employeeDirectory, productCatalog, 
                                                                                                            marketList, orderDirectory, supplierDirectory, 
                                                                                                            productCatalog_Apple, productCatalog_HP, productCatalog_Dell);
                userProcessControl.add("JPanel_ManagerMenu", awajp);
                CardLayout layout = (CardLayout)userProcessControl.getLayout();
                layout.next(userProcessControl);
                
        }else{
            JOptionPane.showMessageDialog(null, "Wrong account!");
        }
    }//GEN-LAST:event_btn_LoginActionPerformed

    /**
     * @param args the command line arguments
     */ public Date getRondomDate(String str_StartDate, String str_EndDate) {  
        Date date = randomDate(str_StartDate, str_EndDate);  
        //new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(date)
        return date;
    }  
     
         private static Date randomDate(String beginDate,String endDate){  
        try {  
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
            Date start = format.parse(beginDate);  
            Date end = format.parse(endDate);  
              
            if(start.getTime() >= end.getTime()){  
                return null;  
            }  
              
            long date = random(start.getTime(),end.getTime());  
              
            return new Date(date);  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return null;  
    } 
         
    private static long random(long begin,long end){  
        long rtn = begin + (long)(Math.random() * (end - begin));  
        if(rtn == begin || rtn == end){  
            return random(begin,end);  
        }  
        return rtn;  
    }      
         
    public static int getRandom(int min, int max){
        Random random = new Random();
        int s = random.nextInt(max) % (max - min + 1) + min;
        return s;
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Login;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel_Left;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTextField txt_managerPassword;
    private javax.swing.JTextField txt_managerUsername;
    private javax.swing.JPanel userProcessControl;
    // End of variables declaration//GEN-END:variables
}
