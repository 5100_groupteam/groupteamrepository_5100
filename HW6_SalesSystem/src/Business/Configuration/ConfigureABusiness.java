/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Configuration;

import Business.Business;
import Class.Person.Person;
import Class.Person.PersonDirectory;
import Class.Person.UserAccount;
import Class.Person.UserAccountDirectory;

/**
 *
 * @author ftl
 */
public class ConfigureABusiness{
    public static Business Initialize (String n) { // returns a business object
        Business b = new Business(n);
        b.setPersonDir(new PersonDirectory());
        b.setUaDir(new UserAccountDirectory());
        
        PersonDirectory pd = b.getPersonDir();
        Person p = pd.addPerson(); //create person object 
        p.setFirstName("Ann");
        p.setLastName("Wells");
        p.setDobMonth("Oct");
        p.setDobDay(15);
        p.setDobYear(1980);
        p.setSsn("123-45-6789");
        p.setAddress("Huntington Ave");

        p = pd.addPerson(); // create a second person object 
        p.setFirstName("John");
        p.setLastName("Brown");
        p.setDobMonth("Mar");
        p.setDobDay(5);
        p.setDobYear(1984);
        p.setSsn("987-65-4321");
        p.setAddress("Huntington Ave");

        UserAccountDirectory uad= b.getUaDir(); // prepare to create user accounts
        Person p2 = pd.findPersonByLastName("Brown");
        
        if(p2 != null) {
            UserAccount ua = uad.addUserAccount();
            ua.setPerson(p2); //link user account to the Mr. Brown 
            ua.setUserId("jadam");
            
            ua.setPassword(EncryptPassword.md5("pw"));
            ua.setAccountType("System Admin");
            ua.setStatus(true);
        }
        
        Person p3 = pd.findPersonByLastName("Wells");
        if(p3 != null) {
            UserAccount ua = uad.addUserAccount();
            ua.setPerson(p3); //link user account to the Mr. Brown 
            ua.setUserId("ahr");
            ua.setPassword(EncryptPassword.md5("pw"));
            ua.setAccountType("HR Admin");
            ua.setStatus(true);
            
            UserAccount ua2 = uad.addUserAccount();
            ua2.setPerson(p3); //link user account to the Mr. Brown 
            ua2.setUserId("ahr2");
            ua2.setPassword(EncryptPassword.md5("pw"));
            ua2.setAccountType("HR Admin");
            ua2.setStatus(true);
        }
        
    return b;
    } 
}
