/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Class.Market.MarketList;
import Class.Market.MarketOfferCatalog;
import Class.Person.EmployeeDirectory;
import Class.Supplier.SupplierDirectory;

/**
 *
 * @author Zi Wei Fan
 */
public class Business {
    private SupplierDirectory supplierDirectory;
    private MarketList marketList;
    private MarketOfferCatalog marketOfferCatalog;
    private EmployeeDirectory customerDirectory;
   
    public Business() {
        this.supplierDirectory = supplierDirectory;
        this.marketList = marketList;
        this.marketOfferCatalog = marketOfferCatalog;
        this.customerDirectory = customerDirectory;
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public MarketList getMarketList() {
        return marketList;
    }

    public void setMarketList(MarketList marketList) {
        this.marketList = marketList;
    }

    public MarketOfferCatalog getMarketOfferCatalog() {
        return marketOfferCatalog;
    }

    public void setMarketOfferCatalog(MarketOfferCatalog marketOfferCatalog) {
        this.marketOfferCatalog = marketOfferCatalog;
    }

    public EmployeeDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(EmployeeDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public SupplierDirectory CreateSupplierDirectory(){
        SupplierDirectory supplierDirectory = new SupplierDirectory();
        return supplierDirectory;
    }
    
    public MarketList CreateMarketList(){
        MarketList marketList = new MarketList();
        return marketList;
    }
    
    public MarketOfferCatalog CreateMarketOfferCatalog(){
        MarketOfferCatalog marketOfferCatalog = new MarketOfferCatalog();
        return marketOfferCatalog;
    }
    
    public EmployeeDirectory CreateCustomerDirectory(){
        EmployeeDirectory customerDirectory = new EmployeeDirectory();
        return customerDirectory;
    }
    
    public EmployeeDirectory CreateEmployeeDirectory(){
        EmployeeDirectory employeeDirectory = new EmployeeDirectory();
        return employeeDirectory;
    }
    
    public int getSupplierDirectorySize(){
        return supplierDirectory.getInt_SupplierDirectorySize();
    }
    
    public int getMarketListSize(){
        return marketList.getMarketListSize();
    }
    
    public int getMarketOfferCatalogSize(){
        return marketOfferCatalog.getMarketOfferCatalogSize();
    }
    
    public int getCustomerDirectorySize(){
        return customerDirectory.getInt_SalesPersonCatalogSize();
    }
    
    public int getEmployeeDirectorySize(){
        return customerDirectory.getInt_SalesPersonCatalogSize();
    }
}
