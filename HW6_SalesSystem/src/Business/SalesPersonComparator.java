/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Class.Person.SalesPerson;
import java.util.Comparator;

/**
 *
 * @author Zi Wei Fan
 */
public class SalesPersonComparator implements Comparator<SalesPerson>{
    public int compare(SalesPerson s1, SalesPerson s2) {
        return Integer.compare(s2.getInt_RevenueTotal(), s1.getInt_RevenueTotal());
    }

}
